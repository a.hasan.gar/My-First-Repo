#include <bits/stdc++.h>

#define FAST_IO ios_base::sync_with_stdio(0); \
				cin.tie(0); \
				cout.tie(0)
#define pb push_back
#define fi first
#define se second
#define mp make_pair
#define all(_v)				_v.begin(), _v.end()
#define sz(_v)				(int) _v.size()
#define FIND(_obj, _val)	(_obj.find(_val) != _obj.end())
#define RESET(_a, _v)		fill_n(_a,sizeof(_a)/sizeof(_a[0]),_v)
#define REP(_i, _n)			for (int _i = 0; _i < (int) _n; _i++)
#define FOR(_i, _a, _b)		for (int _i = (int) _a; _i <= (int) _b; _i++)
#define FORD(_i, _a, _b)	for (int _i = (int) _a; _i >= (int) _b; _i--)
#define FORIT(_it, _obj)	for (auto _it = _obj.begin(); _it != _obj.end(); _it++)

// DEBUG UTIL
#define DEBUG(x) cerr << "> " << #x << " = " << x << endl

using namespace std;

using ll = long long;
using ii = pair<int,int>;
using pll = pair<ll,ll>;
using pdd = pair<double,double>;
using vi = vector<int>;
using vii = vector<ii>;
using vs = vector<string>;

const double PI = acos(-1.0);
const double EPS = 1e-14;
const int MOD = 1e9 + 7;
const int INF = 1e9;
const ll INFLL = 4e18;
const int MAX = 1e5;
const int DX = {0, -1, 0, 1}; // ccw
const int DY = {-1, 0, 1, 0}; // ccw

// EXPONENTIATION BY SQUARING (a^n)
ll power(ll a, ll p) {
	if (p == 0) return 1;
	ll ret = power(a, p/2);
	ret *= ret;

	return (p&1 ? ret*a : ret);
}

// MODULAR EXPONENTIATION O(Log p)
ll modPow(ll a, ll p) {
	if (p == 0) return 1LL;

	a %= MOD;
	ll ret = modPow(a, p/2);
	ret = (ret * ret) % MOD;

	return (p&1 ? (ret*a)%MOD : ret);
}

// INVERSE MOD O(Log mod)
// MOD is a prime number (generally 1e9 + 7)
ll modInverse(int a) {
	assert(__gcd(a, MOD) == 1);

	return modPow(a, MOD-2);
}

// DISJOINT-SET UNION (DSU)
int par[MAX+5];

void init(int n) {
	FOR(i,1,n) {
		par[i] = i;
	}
}

int get_par(int x) {
	if (par[x] != x) par[x] = get_par(par[x]);
	return par[x];
}

bool is_united(int a, int b) {
	return get_par(a) == get_par(b);
}

void unite(int a, int b) {
	a = get_par(a);
	b = get_par(b);
	par[a] = b;
}

// KAHN'S TOPOLOGICAL SORT
int N, M;
vi adj[MAX+5];
int degree[MAX+5];

vi toposort() {
	RESET(degree, 0);
	FOR(i,1,N)
		for (int f : adj[i])
				degree[f]++;

	priority_queue<int, vi, greater<int>> pq;
	FOR(i,1,N)
		if (degree[i] == 0)
			pq.push(i);

	vi ret;
	while (!pq.empty()) {
		int cur = pq.top();
		pq.pop();

		ret.pb(cur);
		for (int f : adj[cur])
			if (--degree[f] == 0)
				pq.push(f);
	}

	// if (sz(ret) < N) -> there's a cycle
	return ret;
}

// SSSP DIJKSTRA O(E Log E)
int N, M;
vii adj[MAX+5]; // adj, weight
int dist[MAX+5];

void dijkstra(int cur) {
	RESET(dist, INF);

	priority_queue<ii, vii, greater<ii>> pq;
	dist[cur] = 0;
	pq.push({dist[cur], cur});

	while (!pq.empty()) {
		cur = pq.top().se;
		int d = pq.top().fi;
		pq.pop();
		if (d > dist[cur]) continue;

		for (ii p_adj : adj[cur]) {
			int f = p_adj.fi;
			int w = p_adj.se;

			if (dist[cur] + w < dist[f]) {
				pq.push({dist[f] = dist[cur] + w, f});
			}
		}
	}
}

// APSP FLOYD-WARSHALL O(V^3)
// init 0 for self-loop, otherwise INF
// input override distance u->v
int N, adjMat[MAX+5][MAX+5];

void apsp() {
	FOR(k,1,N) 
		FOR(i,1,N)
			FOR(j,1,N)
				adjMat[i][j] = min(adjMat[i][j], adjMat[i][k] + adj[k][j]);
}

// LEFT-CHILD RIGHT-SIBLING REPRESENTATION (LCRS)
vi adj[MAX+5];
int l[MAX+5], r[MAX+5]; // -1 to denote nothing

void lcrs(int cur, int from) {
    if (sz(adj[cur]) == 1 && from != -1) {
        l[cur] = -1;
        return;
    }

    int tmp, idx;
    if (adj[cur][0] != from) {
        tmp = adj[cur][0];
        idx = 1;
    }
    else {
        tmp = adj[cur][1];
        idx = 2;
    }
    l[cur] = tmp;
    lcrs(tmp, cur);

    FOR(i,idx,sz(adj[cur])-1) {
        int tmp2 = adj[cur][i];
        if (tmp2 != from) {
            r[tmp] = tmp2;
            tmp = tmp2;
            lcrs(tmp, cur);
        }
    }

    r[tmp] = -1;
}

int main()
{
	FAST_IO;
	int TC = 1;
	while (TC--) {
		read();
		solve();
	}
} 