#include <bits/stdc++.h>

#define FAST_IO ios_base::sync_with_stdio(0); \
				cin.tie(0); \
				cout.tie(0)
#define pb push_back
#define fi first
#define se second
#define mp make_pair
#define all(_v)				_v.begin(), _v.end()
#define sz(_v)				(int) _v.size()
#define FIND(_obj, _val)	(_obj.find(_val) != _obj.end())
#define RESET(_a, _v)		fill_n(_a,sizeof(_a)/sizeof(_a[0]),_v)
#define REP(_i, _n)			for (int _i = 0; _i < (int) _n; _i++)
#define FOR(_i, _a, _b)		for (int _i = (int) _a; _i <= (int) _b; _i++)
#define FORD(_i, _a, _b)	for (int _i = (int) _a; _i >= (int) _b; _i--)
#define FORIT(_it, _obj)	for (auto _it = _obj.begin(); _it != _obj.end(); _it++)

// DEBUG UTIL
#define DEBUG(x) cerr << "> " << #x << " = " << x << endl

using namespace std;

using ll = long long;
using pii = pair<int,int>;
using pll = pair<ll,ll>;
using pdd = pair<double,double>;
using vi = vector<int>;
using vii = vector<pii>;
using vs = vector<string>;

const double PI = acos(-1.0);
const double EPS = 1e-14;
const int MOD = 1e9 + 7;
const int INF = 1e9;
const ll INFLL = 4e18;
const int MAX = 2500;

int N, M, Q;
ll BIT[MAX+15][MAX+15];
map<pair<pii,pii>, ll> vals;

void solve(int,int,int,int,int);

void read() {
	cin >> N >> M >> Q;

	while (Q--) {
		int t, r1, c1, r2, c2;
		cin >> t >> r1 >> c1 >> r2 >> c2;
		solve(t, r1, c1, r2, c2);
	}
}

void update(int r, int c, ll val) {
	int C;
	for (; r <= N; r += r & -r) {
		C = c;
		for (; C <= M; C += C & -C) {
			BIT[r][C] += val;
		}
	}
}

void add(int r1, int c1, int r2, int c2, ll val) {
	update(r1, c1, val);
	update(r1, c2+1, -val);
	update(r2+1, c1, -val);
	update(r2+1, c2+1, val);
}

ll query(int r, int c) {
	ll ret = 0;
	int C;

	for (; r > 0; r -= r & -r) {
		C = c;
		for (; C > 0; C -= C & -C) {
			ret += BIT[r][C];
		}
	}

	return ret;
}

bool walk(int r1, int c1, int r2, int c2) {
	return query(r1, c1) == query(r2, c2);
}

void solve(int t, int r1, int c1, int r2, int c2) {
	if (t == 1) {
		ll rand_val = 0;
		while (rand_val <= 0) rand_val = 1LL * rand() * rand();
		vals[{{r1,c1}, {r2,c2}}] = rand_val;

		add(r1, c1, r2, c2, rand_val);
	}
	else if (t == 2) {
		ll rem_val = vals[{{r1,c1}, {r2,c2}}];
		add(r1, c1, r2, c2, -rem_val);
	}
	else {
		if (walk(r1, c1, r2, c2)) 
			cout << "Yes\n";
		else
			cout << "No\n";
	}
}

int main() {
	FAST_IO;
	srand(time(0));

	int TC = 1;
	while (TC--) {
		read();
	}
} 