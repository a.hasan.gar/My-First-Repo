#include <bits/stdc++.h>

#define FAST_IO ios_base::sync_with_stdio(0); \
				cin.tie(0); \
				cout.tie(0)
#define pb push_back
#define fi first
#define se second
#define mp make_pair
#define all(_v)				_v.begin(), _v.end()
#define sz(_v)				(int) _v.size()
#define FIND(_obj, _val)	(_obj.find(_val) != _obj.end())
#define RESET(_a, _v)		fill_n(_a,sizeof(_a)/sizeof(_a[0]),_v)
#define REP(_i, _n)			for (int _i = 0; _i < (int) _n; _i++)
#define FOR(_i, _a, _b)		for (int _i = (int) _a; _i <= (int) _b; _i++)
#define FORD(_i, _a, _b)	for (int _i = (int) _a; _i >= (int) _b; _i--)
#define FORIT(_it, _obj)	for (auto _it = _obj.begin(); _it != _obj.end(); _it++)

// DEBUG UTIL
#define DEBUG(x) cerr << "> " << #x << " = " << x << endl

using namespace std;

using ll = long long;
using ii = pair<int,int>;
using pll = pair<ll,ll>;
using pdd = pair<double,double>;
using vi = vector<int>;
using vii = vector<ii>;
using vs = vector<string>;

const double PI = acos(-1.0);
const double EPS = 1e-14;
const int MOD = 1e9 + 7;
const int INF = 1e9;
const ll INFLL = 4e18;
const int MAX = 1e5;

int N, C;
ll BIT1[2*MAX], BIT2[2*MAX];

void init() {
	RESET(BIT1, 0);
	RESET(BIT2, 0);
}	 

void update(ll BIT[], int x, ll val) {
	for (; x <= N; x += x & -x) {
		BIT[x] += val;
	}
}

ll query(ll BIT[], int x) {
	ll ret = 0;
	for (; x > 0; x -= x & -x)
		ret += BIT[x];

	return ret;
}

// return sum [1, x]
ll query(int x) {
	return 1LL * query(BIT1, x) * x - query(BIT2, x);
}

// Add val in [a, b]
void add(int a, int b, int val) {
	update(BIT1, a, val);
	update(BIT1, b+1, -val);
	update(BIT2, a, 1LL * val * (a-1));
	update(BIT2, b+1, -1LL * val * b);
}

// return sum[p, q]
ll get_sum(int p, int q) {
	return query(q) - query(p-1);
}

void read() {
	cin >> N >> C;
	REP(i, C) {
		int c, p, q;
		cin >> c >> p >> q;

		if (c == 0) {
			int val; cin >> val;
			add(p, q, val);
		}
		else 
			cout << get_sum(p, q) << "\n";
	}
}

int main()
{
	FAST_IO;
	int TC = 1;
	cin >> TC;
	while (TC--) {
		init();
		read();
	}
} 